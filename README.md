# MGPUSIM

[![Slack](https://whispering-taiga-44824.herokuapp.com/badge.svg)](https://join.slack.com/t/projectakita/shared_invite/enQtODEzMDcyNzMyNDUyLWQyMWQyODI2NzIxN2Y5YzYzMTZkZDE3MDk4MzM5MDI2OTY0Yzc4OWFkNjlmZmU3MWJjZmEyNjA0YmNjNTY4Mjk)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/akita/gcn3)](https://goreportcard.com/report/gitlab.com/akita/gcn3)
[![Test](https://gitlab.com/akita/gcn3/badges/master/pipeline.svg)](https://gitlab.com/akita/gcn3/commits/master)
[![Coverage](https://gitlab.com/akita/gcn3/badges/master/coverage.svg)](https://gitlab.com/akita/gcn3/commits/master)

MGPUSim is a GPU simulator that models AMD GCN3 ISA based GPUs.
